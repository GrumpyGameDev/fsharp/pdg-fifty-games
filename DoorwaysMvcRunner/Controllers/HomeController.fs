﻿namespace DoorwaysMvcRunner.Controllers

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open MBrace.FsPickler
open System.IO

type HomeController () =
    inherit Controller()
    let saveGame = "test.sav"

    member this.LoadGame (fileName:string) : GameState.GameState =
        try
            let binarySerializer = FsPickler.CreateBinarySerializer()
            use reader = File.OpenRead(fileName)
            binarySerializer.Deserialize<GameState.GameState>(reader)
        with
        | _ ->
            let gameState = 
                GameState.generate 256u
            this.SaveGame fileName gameState
            gameState

    member this.SaveGame (fileName:string) (gameState:GameState.GameState) : unit =
        let binarySerializer = FsPickler.CreateBinarySerializer()
        use writer = File.OpenWrite(fileName)
        binarySerializer.Serialize(writer,gameState)

    member this.Index () =
        let gameState = this.LoadGame (saveGame)
        this.View(DoorwaysMvcRunner.Models.StatusModel.ofGameState(gameState))

    member this.Move(id:string) =
        match CardinalDirection.Direction.tryParse id with
        | Some direction ->
            this.LoadGame (saveGame)
            |> GameState.attemptMove direction
            |> this.SaveGame (saveGame)
        | _ -> ()
        this.RedirectToAction("Index")

    member this.Take(id:uint32) =
        this.LoadGame (saveGame)
        |> GameState.attemptTake id
        |> this.SaveGame (saveGame)
        this.RedirectToAction("Index")

    [<HttpPost>]
    member this.SetMark(model:DoorwaysMvcRunner.Models.SetMarkModel) =
        this.LoadGame (saveGame)
        |> GameState.updateMark (if String.IsNullOrWhiteSpace(model.Mark) then None else (model.Mark |> Some))
        |> this.SaveGame (saveGame)
        this.RedirectToAction("Index")

    member this.EraseMark() =
        this.LoadGame (saveGame)
        |> GameState.updateMark None
        |> this.SaveGame (saveGame)
        this.RedirectToAction("Index")

    member this.Error () =
        this.View();
