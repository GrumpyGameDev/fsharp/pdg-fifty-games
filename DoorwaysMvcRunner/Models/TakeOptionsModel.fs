namespace DoorwaysMvcRunner.Models

open System.Collections.Generic

type TakeOptionModel() =
    member val Index : uint32 = 0u with get, set
    member val ItemName : string = null with get,set


type TakeOptionsModel () =
    member val Options : IEnumerable<TakeOptionModel> = null with get, set
    static member ofGameState (gameState:GameState.GameState) : TakeOptionsModel =
        let options = 
            gameState.cellMap.[gameState.avatar.position].items
            |> List.fold 
                (fun (ix,a) i -> 
                    (ix+1u,List.append a [(ix,i)])) (0u,[])
            |> snd
            |> List.map (fun (ix,i) -> TakeOptionModel(Index=ix, ItemName = i.Name))
            |> Seq.ofList
        TakeOptionsModel(Options=options)
