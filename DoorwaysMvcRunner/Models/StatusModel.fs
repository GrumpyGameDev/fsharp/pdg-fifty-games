namespace DoorwaysMvcRunner.Models

open System.Collections.Generic

type StatusModel() =
    member val Message    : string           = ""                 with get, set
    member val VisitCount : uint32           = 0u                 with get, set
    member val Mark       : string           = null               with get, set
    member val Move       : MoveOptionsModel = MoveOptionsModel() with get, set
    member val Take       : TakeOptionsModel = TakeOptionsModel() with get, set
    member val Inventory  : InventoryModel   = InventoryModel()   with get, set
    member val Creatures  : CreatureModel    = CreatureModel()    with get, set
    static member ofGameState (gameState:GameState.GameState) : StatusModel =
        StatusModel(
            Creatures  = CreatureModel.ofGameState(gameState),
            Message    = gameState.commandResult.Text,
            Move       = MoveOptionsModel.ofGameState(gameState),
            Mark       = (gameState.cellMap.[gameState.avatar.position].mark |> Option.fold (fun _ s -> s) ""),
            VisitCount = gameState.cellMap.[gameState.avatar.position].priorVisits,
            Take       = TakeOptionsModel.ofGameState(gameState),
            Inventory  = InventoryModel.ofGameState(gameState))
