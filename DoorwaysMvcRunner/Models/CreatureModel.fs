namespace DoorwaysMvcRunner.Models

open System.Collections.Generic

type CreatureItemModel() =
    member val CreatureName : string = null with get, set
    member val Body : uint32 = 0u with get, set
    member val MaxBody : uint32 = 0u with get, set
    member val Index : uint32 = 0u with get, set

type CreatureModel () =
    member val Creatures : IEnumerable<CreatureItemModel> = null with get, set
    static member ofGameState (gameState:GameState.GameState) : CreatureModel =
        let creatures = 
            gameState.cellMap.[gameState.avatar.position].creatures
            |> List.fold 
                (fun (ix,a) i -> 
                    (ix+1u,List.append a [(ix,i)])) (0u,[])
            |> snd            
            |> List.map 
                (fun (ix,c) -> 
                    CreatureItemModel(
                        CreatureName=c.creatureType.Name,
                        Body = c.creatureType.Body - c.wounds,
                        MaxBody = c.creatureType.Body,
                        Index = ix
                    ))
            |> Seq.ofList
        CreatureModel(Creatures=creatures)
