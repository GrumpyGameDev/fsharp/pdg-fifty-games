namespace DoorwaysMvcRunner.Models

open System.Collections.Generic

type InventoryItemModel() =
    member val Count : uint32 = 0u with get, set
    member val ItemName : string = null with get,set


type InventoryModel () =
    member val Items : IEnumerable<InventoryItemModel> = null with get, set
    static member ofGameState (gameState:GameState.GameState) : InventoryModel =
        let items = 
            gameState.avatar.items
            |> List.fold 
                (fun (a:Map<Item.Item, uint32>) i -> 
                    if a.ContainsKey(i) then
                        a
                        |> Map.add i (a.[i]+1u)
                    else                
                        a
                        |> Map.add i 1u) Map.empty
            |> Map.toList
            |> List.map (fun (item,count) -> InventoryItemModel(ItemName=item.Name, Count=count))
            |> Seq.ofList
        InventoryModel(Items=items)
