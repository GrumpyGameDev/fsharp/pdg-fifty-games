namespace DoorwaysMvcRunner.Models

type SetMarkModel() =
    member val Mark : string = null with get, set
