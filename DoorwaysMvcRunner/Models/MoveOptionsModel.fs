namespace DoorwaysMvcRunner.Models

open System.Collections.Generic

type MoveOptionModel() =
    member val Direction : CardinalDirection.Direction = CardinalDirection.North with get, set
    member val DirectionName : string = null with get,set
    member val DoorName : string = null with get,set


type MoveOptionsModel () =
    member val Options : IEnumerable<MoveOptionModel> = null with get, set
    static member ofGameState (gameState:GameState.GameState) : MoveOptionsModel =
        let options = 
            gameState.cellMap.[gameState.avatar.position].exits
            |> Map.toList
            |> List.map
                (fun (d,e) -> 
                    MoveOptionModel(Direction = d,DirectionName = d.Name, DoorName = e.Name))
            |> Seq.ofList
        MoveOptionsModel(Options=options)
