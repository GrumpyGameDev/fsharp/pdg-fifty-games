module GameState

open System

let private generateCellMap (random:Random) (cellCount:uint32) : CellMap.CellMap =
    
    let rec generateCells (random:Random) (cellCount:uint32) (cellMap:CellMap.CellMap) : CellMap.CellMap =

        if cellCount=0u then
            cellMap        
        else
            if cellMap |> Map.isEmpty then
                cellMap
                |> Map.add (0,0) (Cell.create())
                |> generateCells random (cellCount-1u) 
            else
                let cellLocation = 
                    CellMap.pickRandomCellLocation random cellMap |> Option.get
                let neighborCells = 
                    CardinalDirection.directions
                    |> List.map (fun d -> (d,(CardinalDirection.step 1 d cellLocation)))
                    |> List.filter (fun (_,p) -> (cellMap |> Map.containsKey p |> not))
                match neighborCells with
                | [] -> 
                    cellMap
                    |> generateCells random cellCount
                | _ -> 
                    let direction, position = 
                        Utility.pickRandomlyFromList random neighborCells |> Option.get
                    let startCell =
                        cellMap.[cellLocation].SetExit direction DoorType.Open
                    let endCell = 
                        Cell.create().SetExit direction.Opposite DoorType.Open
                    cellMap
                    |> Map.add (cellLocation) (startCell)
                    |> Map.add (position) (endCell)
                    |> generateCells random (cellCount-1u)

    let cellMap = 
        Map.empty
        |> generateCells random cellCount
        |> CellMap.generateCellTypes random
        |> CellMap.generateLocksAndKeys random
        |> CellMap.generateCreatures cellCount random
        |> CellMap.generateItems random
    
    //find the most negative x and y
    let offsetX, offsetY =
        ((0,0),cellMap)
        ||> Map.fold (fun (x,y) (kx,ky) _ -> 
            ((if x < kx then x else kx),(if y < ky then y else ky)))

    //adjust so that the minimum x and y are zero
    (Map.empty,cellMap)
    ||> Map.fold (fun acc (x,y) v -> 
        acc
        |> Map.add (x-offsetX,y-offsetY) v)

type GameState = 
    {
        cellMap  : CellMap.CellMap
        avatar : Avatar.Avatar
        commandResult : CommandResult.CommandResult
    }
    static member create (cellMap:CellMap.CellMap, position: Position.Position, random:Random) : GameState =
        {
            cellMap  = cellMap
            avatar = position |> Avatar.create
            commandResult = CommandResult.Message ""
        }
    member x.UpdateCellMap (cellMap:CellMap.CellMap) : GameState =
        {x with cellMap = cellMap}
    member x.UpdateAvatar (avatar:Avatar.Avatar) : GameState =
        {x with avatar = avatar}
    member gameState.AttemptMove (direction:CardinalDirection.Direction) : GameState =
        let cell = 
            gameState.cellMap.[gameState.avatar.position]
        match cell.exits |> Map.tryFind direction with
        | None -> gameState.SetCommandResult(direction |> CommandResult.NoExit |> CommandResult.Move)
        | Some exit when exit.HasLock ->
            //what is the keytype?
            let keyType = exit.GetKeyType |> Option.get
            let key = keyType |> Item.Key
            //does the avatar have such a key?
            if gameState.avatar.HasItem key then
                let updatedAvatar = 
                    gameState.avatar.position 
                    |> CardinalDirection.step 1 direction 
                    |> Avatar.setPosition 
                    <| gameState.avatar 
                    |> Avatar.removeItem key
                    |> snd
                let updatedCell = 
                    cell
                    |> Cell.addVisit
                    |> Cell.setExit direction (keyType |> DoorType.Unlocked)
                let updatedCellMap = 
                    gameState.cellMap 
                    |> CellMap.updateCell gameState.avatar.position updatedCell
                ((gameState.UpdateAvatar updatedAvatar).UpdateCellMap updatedCellMap).SetCommandResult((direction, keyType) |> CommandResult.UnlockedWithKey |> CommandResult.Move)
            else
                gameState.SetCommandResult (direction |> CommandResult.Locked |> CommandResult.Move)
        | _ ->
            {gameState with 
                commandResult = direction |> CommandResult.Moved |> CommandResult.Move
                avatar = gameState.avatar.position |> CardinalDirection.step 1 direction |> Avatar.setPosition <| gameState.avatar;  
                cellMap = gameState.cellMap |> CellMap.updateCell gameState.avatar.position {cell with priorVisits = cell.priorVisits+1u}}
    member x.SetCommandResult (commandResult:CommandResult.CommandResult) : GameState =
        {x with commandResult = commandResult}
    member gameState.UpdateMark (updatedMark:string option) : GameState =
        let cell = gameState.cellMap.[gameState.avatar.position]
        let updatedCellMap =
            gameState.cellMap
            |> Map.add gameState.avatar.position (cell.SetMark(updatedMark))
        (gameState.UpdateCellMap updatedCellMap)
            .SetCommandResult (if updatedMark.IsSome then (CommandResult.SetMark |> CommandResult.Mark) else (CommandResult.ErasedMark |> CommandResult.Mark))
    member gameState.AttemptTake (choice:uint32) : GameState =
        let cell = gameState.cellMap.[gameState.avatar.position]
        cell
        |> Cell.getItemByIndex choice
        |> Option.fold
            (fun a i ->
                let updatedAvatar =
                    a.avatar
                    |> Avatar.addItem i
                let updatedCell = 
                    cell
                    |> Cell.removeItemByIndex choice
                let updatedCellMap =
                    a.cellMap
                    |> CellMap.updateCell a.avatar.position updatedCell
                ((a.UpdateCellMap updatedCellMap).UpdateAvatar updatedAvatar).SetCommandResult (i |> CommandResult.Took |> CommandResult.Take)) (gameState.SetCommandResult (CommandResult.CancelledTake |> CommandResult.Take))

let updateCellMap (cellMap:CellMap.CellMap) (gameState:GameState) : GameState =
    gameState.UpdateCellMap cellMap

let updateAvatar (avatar:Avatar.Avatar) (gameState:GameState) : GameState =
    gameState.UpdateAvatar avatar

let generate (cellCount: uint32) : GameState =
    let random = new System.Random()
    let cellMap = 
        generateCellMap random cellCount
    {
        cellMap = cellMap
        avatar = (CellMap.pickRandomFilteredCellLocation random (fun c -> c.ExitCount > 1u) cellMap) |> Option.get |> Avatar.create
        commandResult = CommandResult.Message ""
    }

let attemptMove (direction:CardinalDirection.Direction) (gameState:GameState) : GameState =
    gameState.AttemptMove direction

let setCommandResult (commandResult:CommandResult.CommandResult) (gameState:GameState) : GameState = 
    gameState.SetCommandResult commandResult

let updateMark (updatedMark:string option) (gameState:GameState) : GameState =
    gameState.UpdateMark updatedMark

let attemptTake (choice:uint32) (gameState:GameState) : GameState =
    gameState.AttemptTake choice