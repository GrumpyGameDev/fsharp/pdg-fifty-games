module Utility

open System

let pickRandomlyFromList (random:Random) (l:'TItem list) : 'TItem option =
    match l with
    | [] -> None
    | x ->
        x
        |> List.sortBy (fun _ -> random.Next())
        |> List.head
        |> Some

let weightedGenerate (random:Random) (table:Map<'TGenerated,uint32>) : 'TGenerated =
    let total = 
        table
        |> Map.fold (fun a _ v -> a + v) 0u
    let generated = random.Next(total |> int) |> uint32
    table
    |> Map.fold 
        (fun (r:'TGenerated option, g:uint32) k v -> 
            match r with
            | Some x -> (Some x, 0u)
            | None -> 
                if v<g then
                    (None, g - v)
                else
                    (Some k, 0u)
            ) (None,generated)
    |> fst
    |> Option.get

let combineGenerators (combinator:('TGenerated * 'TGenerated)->'TGenerated) (first:Map<'TGenerated,uint32>) (second:Map<'TGenerated,uint32>) : Map<'TGenerated, uint32> =
    first
    |> Map.fold 
        (fun a kf vf ->
            second
            |> Map.fold
                (fun a' ks vs ->
                    let k = combinator (kf, ks)
                    let v = vf * vs
                    let previous =
                        if a.ContainsKey(k) then
                            a'.[k]
                        else
                            0u
                    a'
                    |> Map.add k (v + previous)) a) Map.empty

let removeByIndex (index:uint32) (items:'T list) =
    items
    |> List.fold 
        (fun (a:'T list,ix: int option) item -> 
            match ix with
            | Some 0 -> (a,None)
            | None  -> (List.append a [item],None)
            | Some n -> (List.append a [item],(n-1) |> Some)) (List.empty,index |> int |> Some)
    |> fst
