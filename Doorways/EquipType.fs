module EquipType

type EquipType =
    | Head
    | Body
    | OnHand
    | OffHand
    | Feet
    member x.Name =
        match x with
        | Head -> "head"
        | Body -> "body"
        | OnHand -> "on hand"
        | OffHand -> "off hand"
        | Feet -> "feet"
