module Cell

type Cell = 
    {
        mark: string option
        exits:Map<CardinalDirection.Direction, DoorType.DoorType>
        items:Item.Item list
        creatures:Creature.Creature list
        cellType : CellType.CellType
        priorVisits:uint32
    }
    member x.SetCellType (cellType:CellType.CellType) : Cell =
        {x with cellType = cellType}
    member x.SetExit (direction:CardinalDirection.Direction) (door:DoorType.DoorType) : Cell =
        {x with exits = x.exits |> Map.add direction door}
    member x.AddVisit =
        {x with priorVisits = x.priorVisits + 1u}
    member x.AddItem (item:Item.Item) : Cell =
        {x with items = x.items |> List.append [item]}
    member x.ExitCount = x.exits.Count |> uint32
    member x.SetType (cellType:CellType.CellType) : Cell =
        {x with cellType = cellType}
    member x.SetMark (mark:string option) : Cell =
        {x with mark = mark}
    member x.HasApparentExit (direction:CardinalDirection.Direction) : bool =
        match x.exits |> Map.tryFind direction with
        | None -> 
            false
        | Some exit -> 
            match exit with
            | _ -> true
    member x.HasItems : bool =
        x.items.Length > 0
    member x.HasItem (item:Item.Item) : bool =
        x.items
        |> List.exists (fun i->item=i)
    member x.GetItemByIndex (index:uint32) : Item.Item option =
        match x.items.Length with
        | n when n<(index|>int) -> None
        | n ->
            x.items
            |> List.item (index |> int)
            |> Some
    member x.RemoveItemByIndex (index:uint32) : Cell =
        let updatedItems =
            x.items
            |> Utility.removeByIndex index
        {x with items = updatedItems}
    member x.AddCreature (creature:Creature.Creature) : Cell =
        let updatedCreatures =
            [creature]
            |> List.append x.creatures
        {x with creatures = updatedCreatures}



let create() : Cell =
    {
        mark        = None
        exits       = Map.empty
        cellType    = CellType.Passageway
        priorVisits = 0u
        items = []
        creatures = []
    }

let setExit  (direction:CardinalDirection.Direction) (door:DoorType.DoorType) (cell:Cell) : Cell =
    cell.SetExit direction door

let addVisit (cell:Cell) : Cell =
    cell.AddVisit

let addItem (item:Item.Item) (cell:Cell) : Cell =
    cell.AddItem item

let exitCount (cell:Cell) : uint32 =
    cell.ExitCount

let setType (cellType:CellType.CellType) (cell:Cell) : Cell =
    cell.SetType(cellType)

let setMark (mark:string option) (cell:Cell) : Cell =
    cell.SetMark(mark)

let hasApparentExit (direction:CardinalDirection.Direction) (cell:Cell) : bool =
    cell.HasApparentExit(direction)

let hasItems (cell:Cell) : bool =
    cell.HasItems

let hasItem (item:Item.Item) (cell:Cell) : bool =
    cell.HasItem item

let getItemByIndex (index:uint32) (cell:Cell) : Item.Item option=
    cell.GetItemByIndex index

let removeItemByIndex (index:uint32) (cell:Cell) : Cell =
    cell.RemoveItemByIndex index

let addCreature (creature:Creature.Creature) (cell:Cell) : Cell =
    cell.AddCreature creature