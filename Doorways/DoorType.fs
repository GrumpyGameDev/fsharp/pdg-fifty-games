module DoorType

type DoorType =
    | Locked of KeyType.KeyType
    | Unlocked of KeyType.KeyType
    | Open
    member x.Name =
        match x with
        | Open -> "an open passage"
        | Unlocked kt ->
            kt.Name
            |> sprintf "an unlocked %s door"
        | Locked kt ->
            kt.Name
            |> sprintf "a locked %s door"
    member x.HasLock =
        match x with
        | Locked _ -> true
        | _ -> false
    member x.GetKeyType =
        match x with 
        | Locked kt -> kt |> Some
        | Unlocked kt -> kt |> Some
        | _ -> None
        

let name (x:DoorType) = x.Name

let hasLock (x:DoorType) = x.HasLock

let getKeyType (x:DoorType) = x.GetKeyType

