module Creature


type CreatureType =
    | Skeleton
    | Goblin
    | ChaosWarrior
    | Fimir
    | Gargoyle
    | Mummy
    | Orc
    | Zombie
    member x.Name : string =
        match x with
        | Skeleton -> "Skeleton"
        | Zombie -> "Zombie"
        | Goblin -> "Goblin"
        | Fimir -> "Fimir"
        | Orc -> "Orc"
        | Mummy -> "Mummy"
        | ChaosWarrior -> "Chaos Warrior"
        | Gargoyle -> "Gargoyle"

    member x.Attack : Map<uint32,uint32> =
        match x with 
        | Skeleton 
        | Zombie
        | Goblin -> Dice.twoWhiteShields
        | Fimir
        | Orc
        | Mummy -> Dice.threeWhiteShields
        | ChaosWarrior
        | Gargoyle -> Dice.fourWhiteShields

    member x.Defend : Map<uint32,uint32> =
        match x with 
        | Goblin -> Dice.oneBlackShield
        | Orc
        | Skeleton -> Dice.twoBlackShields
        | Fimir
        | Zombie -> Dice.threeBlackShields
        | ChaosWarrior
        | Mummy -> Dice.fourBlackShields
        | Gargoyle -> Dice.fiveBlackShields

    member x.Body : uint32 =
        match x with
        | Orc
        | Zombie
        | Skeleton 
        | Goblin -> 1u
        | Fimir 
        | Mummy -> 2u
        | ChaosWarrior
        | Gargoyle -> 3u

    member x.Mind : uint32 =
        match x with
        | Mummy
        | Zombie
        | Skeleton -> 0u
        | Goblin -> 1u
        | Orc -> 2u
        | ChaosWarrior
        | Gargoyle
        | Fimir -> 3u

    member x.ValidCellTypes : Set<CellType.CellType> =
        match x with
        | Skeleton
        | Goblin -> [CellType.Passageway; CellType.Room] |> Set.ofList

        | Zombie
        | Mummy
        | Orc -> [CellType.Passageway; CellType.Room; CellType.DeadEnd] |> Set.ofList

        | Fimir
        | ChaosWarrior
        | Gargoyle -> [CellType.DeadEnd] |> Set.ofList

    member x.CalculateCount (cellCount:uint32) : uint32 =
        match x with
        | Skeleton -> cellCount/4u
        | Zombie -> cellCount/8u
        | Mummy -> cellCount/16u
        | Goblin -> cellCount/6u
        | Orc -> cellCount/12u
        | Fimir -> cellCount/24u
        | ChaosWarrior -> cellCount/24u
        | Gargoyle -> cellCount/48u

let creatureTypes =
    [
        Skeleton
        Goblin
        ChaosWarrior
        Fimir
        Gargoyle
        Mummy
        Orc
        Zombie
    ]

type Creature =
    {
        creatureType: CreatureType
        wounds: uint32
    }
    static member Create (creatureType:CreatureType) : Creature =
        {
            creatureType = creatureType
            wounds = 0u
        }

let create (creatureType:CreatureType) : Creature =
    creatureType
    |> Creature.Create