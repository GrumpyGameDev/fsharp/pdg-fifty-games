module KeyType

open System

type KeyType =
    | Red
    | Yellow
    | Green
    | Cyan
    | Blue
    | Magenta
    member x.Name =
        match x with
        | Red -> "red"
        | Yellow -> "yellow"
        | Green -> "green"
        | Cyan -> "cyan"
        | Blue -> "blue"
        | Magenta -> "magenta"

let name (kt:KeyType) : string =
    kt.Name

let private keyTypeGenerator =
    [
        (Magenta,1u)
        (Blue,2u)
        (Cyan,4u)
        (Green,8u)
        (Yellow,16u)
        (Red,32u)
    ]
    |> Map.ofList

let generate (random:Random) =
    Utility.weightedGenerate random keyTypeGenerator
