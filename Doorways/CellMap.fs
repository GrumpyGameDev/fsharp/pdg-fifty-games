module CellMap 

open System

type CellMap = Map<Position.Position,Cell.Cell>

let updateCell (position:Position.Position) (cell:Cell.Cell) (cellMap:CellMap) : CellMap =
    cellMap |> Map.add position cell

let generateCellTypes (random:Random) (cellMap:CellMap): CellMap =
    cellMap
    |> Map.map (fun _ v -> v.SetType(CellType.generate random v.ExitCount))

let pickRandomCellLocation (random:Random) (cellMap:CellMap) : Position.Position option=
    cellMap
    |> Map.toList
    |> List.map fst
    |> Utility.pickRandomlyFromList random

let pickRandomFilteredCellLocation (random:Random) (filter:Cell.Cell->bool) (cellMap:CellMap) : Position.Position option=
    cellMap
    |> Map.filter (fun _ v -> v |> filter)
    |> pickRandomCellLocation random

let generateLocksAndKeys (random:Random) (cellMap:CellMap): CellMap =
    cellMap
    |> Map.filter 
        (fun _ v -> v.ExitCount = 1u)
    |> Map.fold (fun a k v->
        let direction = v.exits |> Map.toList |> List.head |> fst
        let k2 = CardinalDirection.step 1 direction k
        a
        |> List.append [(k,direction,k2,direction.Opposite,KeyType.generate random)]) []
    |> List.fold 
        (fun a (k1,d1,k2,d2,kt) ->
            let p = a |> pickRandomFilteredCellLocation random (fun c -> c.cellType<>CellType.DeadEnd) |> Option.get
            let a' =
                a
                |> updateCell k1 (a.[k1] |> Cell.setExit d1 (kt |> DoorType.Unlocked))
                |> updateCell k2 (a.[k2] |> Cell.setExit d2 (kt |> DoorType.Locked))
            //two steps, because p may coincide with either k1 or k2
            a'
            |> updateCell p (a'.[p] |> Cell.addItem (kt |> Item.Key))) cellMap

let generateCreatures (cellCount:uint32) (random:Random) (cellMap:CellMap) : CellMap =
    Creature.creatureTypes
    |> List.map (fun ct -> (ct,ct.CalculateCount cellCount))
    |> Map.ofList
    |> Map.fold 
        (fun a k v -> 
            [1u..v]
            |> List.fold
                (fun a' _ -> 
                    a'
                    |> pickRandomFilteredCellLocation random (fun x -> k.ValidCellTypes.Contains(x.cellType))
                    |> Option.fold (fun a'' p -> 
                        let updatedCell = 
                            a''.[p]
                            |> Cell.addCreature (k |> Creature.create)
                        a''
                        |> Map.add p updatedCell) a') a) cellMap



let generateItems (random:Random) (cellMap:CellMap) : CellMap =
    cellMap
