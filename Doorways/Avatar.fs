module Avatar

type Avatar =
    {
        position: Position.Position
        items: Item.Item list
    }
    member x.SetPosition (position:Position.Position) : Avatar =
        {x with position = position}
    member x.AddItem (item:Item.Item): Avatar =
        {x with items = x.items |> List.append [item]}
    member x.HasItem (item:Item.Item): bool =
        x.items
        |> List.exists (fun item -> item = item)
    member x.RemoveItem (item:Item.Item) : bool * Avatar =
        let updatedItems = 
            x.items
            |> List.fold 
                (fun (a,f) i -> 
                    match f, i=item with
                    | false, true -> (a, true)
                    | _ -> (List.append a [i],f)
                    ) ([],false)
        (updatedItems |> snd, {x with items = updatedItems |> fst})

let create (position:Position.Position) : Avatar =
    {
        position = position
        items = []
    }

let setPosition (position:Position.Position) (avatar:Avatar) : Avatar =
    avatar.SetPosition position

let addItem (item:Item.Item) (avatar:Avatar) : Avatar =
    avatar.AddItem item

let hasItem (item:Item.Item) (avatar:Avatar) : bool =
    avatar.HasItem item

let removeItem (item:Item.Item) (avatar:Avatar) : bool * Avatar =
    avatar.RemoveItem item
