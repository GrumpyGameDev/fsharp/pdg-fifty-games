module Item

type Item =
    | Weapon of WeaponType.WeaponType * uint32
    | Key of KeyType.KeyType
    member x.Name =
        match x with 
        | Weapon (wt, d) ->
            sprintf "%s (%u/%u)" wt.Name (wt.Durability - d) wt.Durability
        | Key kt ->
            kt.Name
            |> sprintf "%s key"

let name (item:Item) : string =
    item.Name