module CellType

open System

type CellType = 
    | Room 
    | Passageway
    | DeadEnd
    member x.Name = 
        match x with
        | Room -> "room"
        | Passageway -> "passageway"
        | DeadEnd -> "dead end"

let name (x:CellType) = 
    x.Name

let private oneExitCellTypeGenerator = 
    [
        (CellType.DeadEnd,1u)
    ]
    |> Map.ofList
let private twoExitCellTypeGenerator = 
    [
        (CellType.Room,1u)
        (CellType.Passageway,2u)
    ]
    |> Map.ofList
let private threeExitCellTypeGenerator = 
    [
        (CellType.Room,2u)
        (CellType.Passageway,1u)
    ]
    |> Map.ofList
let private fourExitCellTypeGenerator = 
    [
        (CellType.Passageway,1u)
    ]
    |> Map.ofList
let private cellTypeGenerator =
    [
        (1u,oneExitCellTypeGenerator)
        (2u,twoExitCellTypeGenerator)
        (3u,threeExitCellTypeGenerator)
        (4u,fourExitCellTypeGenerator)
    ]
    |> Map.ofList

let generate (random:Random) (exitCount:uint32) =
    Utility.weightedGenerate random cellTypeGenerator.[exitCount]
