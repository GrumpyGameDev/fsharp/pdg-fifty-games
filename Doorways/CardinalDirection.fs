﻿module CardinalDirection

type Direction =
    | North
    | East
    | South
    | West
    member x.Opposite =
        match x with 
        | North -> South
        | East -> West
        | South -> North
        | West -> East
    member x.Name =
        match x with 
        | North -> "north"
        | East -> "east"
        | South -> "south"
        | West -> "west"
    static member tryParse (s:string) : Direction option =
        match s.ToLower() with
        | "north" -> North |> Some
        | "south" -> South |> Some
        | "east" -> East |> Some
        | "west" -> West |> Some
        | _ -> None


let opposite (direction:Direction) = direction.Opposite

let step (steps:int) (direction:Direction) (position:Position.Position) : Position.Position =
    match direction with
    | North -> ((position |> fst)        , (position |> snd) - steps) 
    | East  -> ((position |> fst) + steps, (position |> snd)        ) 
    | South -> ((position |> fst)        , (position |> snd) + steps) 
    | West  -> ((position |> fst) - steps, (position |> snd)        ) 
        
let directions = [North;East;South;West]

