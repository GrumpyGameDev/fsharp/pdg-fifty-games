module CommandResult

type MoveResult =
    | NoExit of CardinalDirection.Direction
    | Locked of CardinalDirection.Direction
    | UnlockedWithKey of CardinalDirection.Direction * KeyType.KeyType
    | Moved of CardinalDirection.Direction
    member x.Text =
        match x with
        | NoExit direction -> sprintf "There is no %s exit." direction.Name
        | Locked direction -> sprintf "The %s door is locked." (direction.Name)
        | UnlockedWithKey (direction,keyType) -> sprintf "You unlocked the %s door with a %s key." direction.Name keyType.Name
        | Moved direction -> sprintf "You moved %s." direction.Name

type MarkResult =
    | SetMark
    | ErasedMark
    member x.Text =
        match x with
        | SetMark -> "Set mark."
        | ErasedMark -> "Erased mark."

type TakeResult =
    | NoItems
    | Took of Item.Item
    | CancelledTake
    member x.Text =
        match x with
        | NoItems -> "There were no items to take."
        | Took item -> sprintf "Took %s." item.Name
        | CancelledTake -> "Cancelled taking the item."

type CommandResult =
    | Move of MoveResult
    | Mark of MarkResult
    | Take of TakeResult
    | Message of string
    member x.Update =
        match x with
        | Message "" -> false
        | _ -> true
    member x.Text =
        match x with
        | Message s -> s
        | Move m -> m.Text
        | Take t -> t.Text
        | Mark m -> m.Text