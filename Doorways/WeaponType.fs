module WeaponType

type WeaponType =
    | Dagger
    | ShortSword
    | LongSword
    | TwoHandedSword
    member x.Name =
        match x with
        | Dagger -> "dagger"
        | ShortSword -> "short sword"
        | LongSword -> "long sword"
        | TwoHandedSword -> "two-handed sword"

    member x.DamageDice =
        match x with
        | Dagger -> Dice.oneSkull
        | ShortSword -> Dice.twoSkulls
        | LongSword -> Dice.threeSkulls
        | TwoHandedSword -> Dice.fourSkulls

    member x.EquipSlots =
        match x with
        | TwoHandedSword -> [EquipType.OffHand; EquipType.OnHand] |> Set.ofList
        | _ -> [EquipType.OnHand] |> Set.ofList

    member x.Durability =
        match x with
        | Dagger         ->  25u
        | ShortSword     ->  50u
        | LongSword      ->  75u
        | TwoHandedSword -> 100u
