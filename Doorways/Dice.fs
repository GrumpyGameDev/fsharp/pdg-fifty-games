module Dice

let private dieCombiner = Utility.combineGenerators (fun (a,b) -> a + b)

let oneSkull = 
    [
        (1u,1u)
        (0u,1u)
    ]
    |> Map.ofList
let twoSkulls =
    oneSkull
    |> dieCombiner oneSkull
let threeSkulls =
    twoSkulls
    |> dieCombiner oneSkull
let fourSkulls =
    threeSkulls
    |> dieCombiner oneSkull

let oneWhiteShield =
    [
        (1u,1u)
        (0u,2u)
    ]
    |> Map.ofList
let twoWhiteShields =
    oneWhiteShield
    |> dieCombiner oneWhiteShield
let threeWhiteShields =
    twoWhiteShields
    |> dieCombiner oneWhiteShield
let fourWhiteShields =
    threeWhiteShields
    |> dieCombiner oneWhiteShield

let oneBlackShield =
    [
        (1u,1u)
        (0u,5u)
    ]
    |> Map.ofList
let twoBlackShields =
    oneBlackShield
    |> dieCombiner oneBlackShield
let threeBlackShields =
    twoBlackShields
    |> dieCombiner oneBlackShield
let fourBlackShields =
    threeBlackShields
    |> dieCombiner oneBlackShield
let fiveBlackShields =
    fourBlackShields
    |> dieCombiner oneBlackShield
