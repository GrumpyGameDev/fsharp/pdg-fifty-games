﻿open GameState
open Runner

[<EntryPoint>]
let main _ = 
    64u
    |> GameState.generate
    |> GameState.run
    0
