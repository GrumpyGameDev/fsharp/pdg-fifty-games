module Runner

open GameState
open System.Linq
open System

let private showHelp () : unit =
    printfn ""
    printfn "Commands:"
    // printfn "[A]"
    // printfn "[B]"
    // printfn "[C]"
    printfn "[D]rop (items)"
    printfn "[E]ast"
    // printfn "[F]"
    // printfn "[G]"
    printfn "[H]elp"
    printfn "[I]nventory"
    // printfn "[J]"
    // printfn "[K]"
    // printfn "[L]"
    printfn "[M]ark (write or erase)"
    printfn "[N]orth"
    // printfn "[O]"
    // printfn "[P]"
    printfn "[Q]uit"
    printfn "[R]efresh (screen)"
    printfn "[S]outh"
    printfn "[T]ake (items)"
    // printfn "[U]"
    // printfn "[V]"
    printfn "[W]est"
    // printfn "[X]"
    // printfn "[Y]"
    // printfn "[Z]"

let rec private confirmQuit () : bool =
    printfn ""
    printfn "Are you sure you want to quit?"
    printfn "[Y]es"
    printfn "[N]o"
    match System.Console.ReadLine().ToLower().FirstOrDefault() with
    | 'y' -> true
    | 'n' -> false
    | _ -> confirmQuit()

let private handleMark (gameState:GameState) : GameState =
    let cell = gameState.cellMap.[gameState.avatar.position]
    let updatedMark = 
        match cell.mark with
        | Some _ ->
            None
        | None ->
            printfn ""
            printf "Enter mark text:"
            match Console.ReadLine() with
            | x when String.IsNullOrWhiteSpace(x) -> None
            | x -> x |> Some
    gameState
    |> GameState.updateMark updatedMark

let private handleTake (gameState:GameState) : GameState =
    let cell = gameState.cellMap.[gameState.avatar.position]
    match cell.items with
    | [] -> 
        printfn ""
        printfn "There are no items to take."
        gameState
        |> GameState.setCommandResult (CommandResult.NoItems |> CommandResult.Take)
    | items -> 
        printfn ""
        printfn "Items:"
        items
        |> List.fold 
            (fun a item -> 
                printfn "[%d] %s" a item.Name
                (a+1)) 0
        |> ignore
        printfn "Which item to take?"
        match UInt32.TryParse(Console.ReadLine()) with
        | true, choice ->
            gameState
            |> GameState.attemptTake choice
        | _ -> 
            gameState
            |> GameState.setCommandResult(CommandResult.CancelledTake |> CommandResult.Take)

let private showInventory (gameState:GameState) : unit =
    printfn ""
    if gameState.avatar.items.IsEmpty then
        printfn "You have no items"
    else
        printfn "Items in inventory:"
        gameState.avatar.items
        |> List.fold
            (fun (a:Map<ItemType.ItemType,uint32>) i -> 
                if a.ContainsKey(i) then
                    a
                    |> Map.add i (a.[i]+1u)
                else
                    a
                    |> Map.add i 1u) Map.empty
        |> Map.iter
            (fun k v ->
                printfn "%s x %u" k.Name v)
    
        

let rec private run (gameState:GameState) : unit = 
    if gameState.commandResult.Update then
        let cell = 
            gameState.cellMap.[gameState.avatar.position]
        printfn ""
        printfn "You are in a %s." (cell.cellType |> CellType.name)
        printfn "Exits:"
        cell.exits
        |> Map.iter (fun k v -> 
                (k.Name,v.Name)
                ||>printfn ("%s - %s"))
        printfn "You have been here %d times previously." (cell.priorVisits)
        if cell.HasItems then
            printfn "There are items here."
        cell.mark
        |> Option.iter (fun mark -> mark |> printfn "You have written a mark that reads '%s'.")

    match System.Console.ReadLine().ToLower().FirstOrDefault() with
    | 'q' -> 
        if confirmQuit() then
            ()
        else
            gameState
            |> GameState.setCommandResult CommandResult.Refresh
            |> run
    | 't' ->
        gameState
        |> handleTake
        |> run

    | 'r' ->
        gameState
        |> GameState.setCommandResult CommandResult.Refresh
        |> run

    | 'i' ->
        gameState
        |> showInventory

        gameState
        |> run

    | 'm' ->
        gameState 
        |> handleMark
        |> run

    | 'h' ->
        showHelp()
        run gameState

    | 'n' -> 
        (CardinalDirection.North, gameState)
        ||> attemptMove
        |> run

    | 'e' -> 
        (CardinalDirection.East, gameState)
        ||> attemptMove
        |> run

    | 's' -> 
        (CardinalDirection.South, gameState)
        ||> attemptMove
        |> run

    | 'w' -> 
        (CardinalDirection.West, gameState)
        ||> attemptMove
        |> run

    | _ -> 
        gameState 
        |> GameState.setCommandResult CommandResult.NoRefresh 
        |> run


type GameState with
    static member run (gameState:GameState) : unit = 
        gameState
        |> run

